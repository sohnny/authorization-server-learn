package com.xueliman.iov.server.web.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author zxg
 */
public interface UserService extends UserDetailsService {
}
