package com.xueliman.iov.user_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @author zxg
 */
@EnableFeignClients(basePackages = "com.xueliman.iov.**")
@SpringBootApplication(scanBasePackages = "com.xueliman.iov.**")
public class UserServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserServerApplication.class, args);
    }
}
