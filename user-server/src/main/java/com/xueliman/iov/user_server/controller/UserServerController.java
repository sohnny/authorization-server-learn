package com.xueliman.iov.user_server.controller;

import cn.hutool.core.lang.ClassScanner;
import com.xueliman.iov.cloud.framework.web.vo.SingleResultBundle;
import com.xueliman.iov.resource_feign.client.ResourceFeign;
import com.xueliman.iov.resource_feign.dto.TestDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

/**
 * @author zxg
 */
@Api(tags = "用户资源服务器")
@Slf4j
@RestController
public class UserServerController {
    @Autowired
    private ResourceFeign resourceFeign;

    @Value("${zxg.zzz}")
    private String aaa;

    @ApiOperation(value = "获取测试")
    @GetMapping("/getResourceTest")
    public SingleResultBundle<String> getResourceTest(){
        return SingleResultBundle.success(resourceFeign.getResourceTest());
    }

    @ApiOperation(value = "获取测试2")
    @GetMapping("/getResourceTest2")
    public SingleResultBundle<String> getResourceTest2(){
        return SingleResultBundle.success(resourceFeign.getResourceTest2());
    }

    @ApiOperation(value = "获取测试2")
    @GetMapping("/getResourceTest3")
    public SingleResultBundle<Integer> getResourceTest3(){
        return SingleResultBundle.success(resourceFeign.getResourceTest3(new TestDTO()));
    }

    @ApiOperation(value = "我的测试")
    @GetMapping("/myTest")
    public SingleResultBundle<String> myTest(String packageName){

        Set<Class<?>> classes = ClassScanner.scanAllPackageByAnnotation(packageName, FeignClient.class);

        for (Class<?> aClass : classes) {
            System.out.println(aClass.getClass().getName());
        }

        return SingleResultBundle.success("这是myTest()" + aaa);
    }
}
